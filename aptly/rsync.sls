create_repo_dir:
  file.directory:
    - user: aptly
    - name: /srv/pkgrepo
    - mode:  755

rsync:
  cmd.run:
    - name: rsync --exclude=.ssh/* -a --delete-before -e ssh aptly@repo1.dev.infra.deposit:/srv/pkgrepo/public/ /srv/pkgrepo/public/
    - runas: aptly
