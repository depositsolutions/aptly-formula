{% from "aptly/map.jinja" import aptly with context %}

include:
  - aptly.publish_repos
  - aptly.publish_mirrors_snapshots

update_patchlevel:
  cmd.run:
    - name: echo $(($(cat {{ aptly.patchlevel_file }})+1)) > {{ aptly.patchlevel_file }}
    - runas: aptly
    - env:
    - HOME: {{ aptly.homedir }}

email:
  cmd.run:
    - name: |
        REPO_OUTPUT=""; for i in $(aptly snapshot list|grep -v old|grep '*' | awk -F'[' '{ print $2 }'|awk -F ']' '{print $1}' | sed 's/\-latest//g'); do REPO_OUTPUT="$REPO_OUTPUT\n[$i]\n $(aptly snapshot diff $i-latest $i-old)"; done; echo -e $REPO_OUTPUT;
        PATCHLEVEL=$(cat {{ aptly.patchlevel_file }})
        echo -e "Dear Infra team,\n\n" \
        "The internal package repository and its mirrors was updated and is now live.\n" \
        "PATCHLEVEL: $PATCHLEVEL\n\n" \
        "Diffs:\n\n" \
        "$REPO_OUTPUT\n\n" \
        "Xoxo,\n" \
        "Aptly" | mail -n -s "[APTLY] Internal Repository Updated - Patchlevel $PATCHLEVEL" {{ aptly.mail_to }}
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.publish_mirrors_snapshots
