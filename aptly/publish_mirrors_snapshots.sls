# Set up our Aptly mirrors

{% from "aptly/map.jinja" import aptly with context %}

include:
  - aptly
  - aptly.aptly_config
  - aptly.create_mirrors 

{% set mirror = aptly.default_mirror %}
{% if salt['pillar.get']('aptly:mirrors') %}
{% for mirror, opts in salt['pillar.get']('aptly:mirrors').items() %}
  {% set mirrorloop = mirror %}

drop_latest_{{ mirror }}_publish_if_exists:
  cmd.run:
    - name: aptly publish drop {{ mirror }}-latest
    - onlyif: aptly publish show {{ mirror }}-latest
    - runas: aptly
    - env:
    - require:
      - sls: aptly.create_mirrors

publish_latest_{{ mirror }}_snapshot:
  cmd.run:
    - name: aptly publish snapshot -label="$(cat {{ aptly.patchlevel_file }})" -distribution={{ mirror }} {{ mirror }}-latest
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.create_mirrors

  {% endfor %}
{% endif %}
